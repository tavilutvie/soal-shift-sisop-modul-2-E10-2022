#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <string.h>
#include <dirent.h>

struct film {
  	char nama[100];
  	int tahun;
  	char kategori[100];
} data[100];

void ASCsort(struct film data[100], int s)
{
    int i, j;
    struct film temp;
    
    for (i = 0; i < s - 1; i++)
    {
        for (j = 0; j < (s - 1-i); j++)
        {
            if (data[j].tahun > data[j + 1].tahun)
            {
                temp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = temp;
            } 
        }
    }
}

void unzip() {
	pid_t child_id;
    int status;
    child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"unzip", "-q", "drakor.zip", "*.png", "-d", "/home/tavilutvie/shift2/drakor", NULL};
		execv("/bin/unzip", argv);
	} 
    else {
		((wait(&status)) > 0);
	}
}

char *strremove(char *str, const char *sub) {
    size_t len = strlen(sub);
    if (len > 0) {
        char *p = str;
        while ((p = strstr(p, sub)) != NULL) {
            memmove(p, p + len, strlen(p + len) + 1);
        }
    }
    return str;
}

void create_folder_recursive(char nama[],char kategori[], char gambar[]){
    char newpath[1000];
	char originpath[1000];

	strcpy(newpath, "/home/tavilutvie/shift2/drakor/");
	strcpy(originpath, "/home/tavilutvie/shift2/drakor/");
	strcat(newpath, kategori);

    pid_t child_id;
    int status;
    child_id = fork();
    
    if (child_id == 0){
        char *argv[] = {"mkdir", "-p", newpath, NULL};
        execv("/bin/mkdir", argv);
    }
    else{
        ((wait(&status)) > 0);
    }

	strcat(originpath,gambar);
	strcat(newpath, "/");
	strcat(newpath, nama);
	strcat(newpath, ".png");

	child_id = fork();

	if (child_id == 0){
        char *argv[] = {"cp", "-b", originpath, newpath, NULL};
        execv("/bin/cp", argv);
    }
    else{
		((wait(&status)) > 0);
	}
}

void add_data2(char nama[],int tahun,char kategori[]){
    FILE * fp;
	FILE * file;
    char path[1000];

    strcpy(path, "/home/tavilutvie/shift2/drakor/");
    strcat(path, kategori);
    strcat(path, "/data.txt");

	if (file = fopen (path,"r+")) {
		fclose (file);
	}
	else{
		fp = fopen (path,"w+");	
    	fprintf(fp,"kategori : %s\n\n",kategori);

    	fclose (fp);
	}
    fp = fopen (path,"a+");
    fprintf(fp,"nama : %s\nrilis : tahun %d\n\n",nama,tahun);
    fclose (fp);
}

void add_data1(struct film data[100], int m){
	for(int i=0;i<m;i++){
		add_data2(data[i].nama, data[i].tahun, data[i].kategori);
	}
}

void delete_image(char gambar[]) {
	char path[1000];

	strcpy(path, "/home/tavilutvie/shift2/drakor/");
	strcat(path, gambar);

	pid_t child_id;
    int status;
    child_id = fork();

	if (child_id == 0) {
		char *argv[] = {"rm", "-f", path, NULL};
        execv("/bin/rm", argv);
	} 
    else {
		((wait(&status)) > 0);
	}
}

void body_function() {
    char path[] = "/home/tavilutvie/shift2/drakor/";
	struct dirent *dp;
	DIR *dir = opendir(path);
    int m=0;
    while ((dp = readdir(dir)) != NULL) {
		
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char gambar[1000];
            strcpy(gambar, dp->d_name);
        	strremove(gambar, ".png");
       	 
        	char nama[100], tahun[100], kategori[100];
			int index;
            char *word = strtok(gambar, "_;");
            index = 0;
            while(word != NULL) {
				if(index==3){
					index=0;
				}
                if(index == 2) {
					strcpy(kategori, word);
                    create_folder_recursive(nama, kategori, dp->d_name);

					strcpy(data[m].nama, nama);
					data[m].tahun = atoi(tahun);
					strcpy(data[m].kategori, kategori);
					m++;
                }
				else if(index == 0) {
                 	strcpy(nama, word);
               	}
               	else if(index == 1) {
                 	strcpy(tahun, word);
               	}
               	word = strtok(NULL, "_;");
               	index++;
        	}
			delete_image(dp->d_name);
        }
    }
	ASCsort(data, m);
	add_data1(data, m);
}

int main(){
    pid_t child_id;
	int status;
	child_id = fork();

  	if (child_id == 0){
        char *argv[] = {"mkdir", "-p", "/home/tavilutvie/shift2/drakor", NULL};
    	execv("/bin/mkdir", argv);
    }
	else{
    	((wait(&status)) > 0);
    }
    unzip();
    body_function();
}