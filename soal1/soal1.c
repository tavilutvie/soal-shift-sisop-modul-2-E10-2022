#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <syslog.h>
#include <json-c/json.h>
#include <fcntl.h>
#include <errno.h>
#define PATH_MAX 64


int main(){
    pid_t pid, sid;

    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    if((chdir("/home/hafizen/modul2/soal1")) < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    while(1){
        int status;

        time_t now;
        now = time(NULL);
        struct tm * getTime = localtime(&now);

        if(
            getTime->tm_mday == 30 &&
            getTime->tm_mon  == 2 &&
            getTime->tm_hour == 4 &&
            getTime->tm_min  == 44
        ){
      pid_t pid1, pid2;
      int process = 0; 
      pid = fork();
      process++;

      if (pid1 == 0){
        char *args[] = {"wget", "-P","./", "-o-", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "characters", NULL };
        execv("/bin/wget", args);
      }

      else {
        pid2= fork();
        process++;

        if (pid2== 0){
          char *args[] = {"wget", "-P","./", "-o-", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons", NULL };
          execv("/bin/wget", args);
        }

        else {
          for(int i = 0; i < process; i++){
            while ((wait(&status)) > 0);
          }
          process = 0;

          pid_t pid3, pid4, pid5;
          pid3 = fork();
          process++;

          if(pid3 == 0){
            char *args[] = {"unzip", "-q", "characters", NULL };
            execv("/bin/unzip", args);
          }

          else{
            pid4 = fork();
            process++;

            if(pid4 == 0){
              char *args[] = {"unzip", "-q", "weapons", NULL };
              execv("/bin/unzip", args);
            }

            else {
              pid5 = fork();
              process++;

              if(pid5 == 0){
                char *args[] = {"mkdir", "gacha_gacha", NULL };
                execv("/bin/mkdir", args);
              }
              else {
                for(int i = 0; i < process; i++){
                  while ((wait(&status)) > 0);
                }
                process = 0;

                if (remove("characters.zip") == 0){
                  if(remove("weapons.zip") == 0){
                    sleep(1);
                }

                chdir("./gacha_gacha");
              }
            }
          }
        }
      }

      int total_json_char = 0, total_json_weap = 0, num;
      char ch, path[PATH_MAX], curr_dir[PATH_MAX];
      FILE *file_char, *file_weap;

      file_char = popen("ls ../characters", "r");
      while((ch=fgetc(file_char))!=EOF) {
        if(ch=='\n')
          total_json_char++;
      }

      file_weap = popen("ls ../weapons", "r");
      while((ch=fgetc(file_weap))!=EOF) {
        if(ch=='\n')
          total_json_weap++;
      }

      int gacha_counter = 0, gacha_taken, primogems = 79000;
      gacha_taken = primogems / 160;

      for(int i = 0; i < gacha_taken; i++){
        gacha_counter++;
        primogems -= 160;

        FILE *roll;
        char str_dir[PATH_MAX], str_txt[130], str_temp[PATH_MAX];
        char *buffer;
        buffer = (char *) malloc(3072);

        struct json_object *parsed_json;
        struct json_object *rarity; 
        struct json_object *name;

        if(gacha_counter % 90 == 1){
          pid_t dir_gacha;
          dir_gacha = fork();

          memset(str_dir, 0, sizeof str_dir);
          int int_count = ((gacha_counter / 90) + 1) * 90;
          sprintf(str_dir,"./total_gacha_%d", int_count);

          if(dir_gacha == 0){
            char *args[] = {"mkdir", str_dir, NULL };
            execv("/bin/mkdir", args);
          }
          else {
            while ((wait(&status)) > 0);
          }

        }

        if(gacha_counter % 10 == 1){
          time_t rawtime;
          struct tm * timeinfo;
      
          time(&rawtime);
          timeinfo = localtime(&rawtime);

          memset(str_txt, 0, sizeof str_txt);
          int int_count = ((gacha_counter / 10) + 1) * 10;
          strcat(str_txt, str_dir);
          sprintf(str_temp, "/%02d:%02d:%02d_gacha_%d.txt", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, int_count);
          strcat(str_txt, str_temp);
          sleep(1);
        }

        FILE *txt_gacha = fopen(str_txt, "a");
        
        if(gacha_counter%2 == 0){
          
          num = (rand() % (total_json_weap));
          if (num == 0) num++;
          file_weap = popen("find ../weapons", "r");
          for(int i = 0; i < total_json_weap; i++){
            fgets(path, PATH_MAX, file_weap);
            if(i == num) break;
          }

          long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);

          fprintf(txt_gacha, "%d_weapon_%d_%s_%d\n", gacha_counter, json_object_get_int(rarity), json_object_get_string(name), primogems);
        }
        else {
          num = (rand() % (total_json_char));
          if (num == 0) num++;
          file_char = popen("find ../characters", "r");
          for(int i = 0; i < total_json_char; i++){
            fgets(path, PATH_MAX, file_char);
            if(i == num) break;

          long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);
          
          fprintf(txt_gacha, "%d_character_%d_%s_%d\n", gacha_counter, json_object_get_int(rarity), json_object_get_string(name), primogems);
        }

        fclose(txt_gacha);
      }
      fclose(file_char);
      fclose(file_weap);

      sleep(3 * 3600);

      chdir("..");
      
      pid_t pid6, pid7, pid8, pid9;
      pid6 = fork();
      if(pid6 == 0){
        char *args[] = {"zip", "-q", "-r", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL };
        execv("/bin/zip", args);
      }
      else{
        while ((wait(&status)) > 0);
  
        pid7 = fork();
        if(pid7 == 0){
          char *args[] = {"rm", "-rf", "characters", NULL };
          execv("/bin/rm", args);
        }
        else {
          pid8 = fork();
          if(pid8 == 0){
            char *args[] = {"rm", "-rf", "weapons", NULL };
            execv("/bin/rm", args);
          }
          else {
            pid9 = fork();
            char *args[] = {"rm", "-rf", "gacha_gacha", NULL };
            execv("/bin/rm", args);
          }
        }
      }
      while(wait(&status) > 0);
      }
    }
  }
}