# SISOP Modul 2 (Kelompok E10)
**soal-shift-sisop-modul-2-E10-2022**

## Anggota Kelompok
No|Nama|NRP
-- | ---------- | -------------
1|Hafiz Kurniawan|(5025201032)
2|Eldenabih Tavirazin Lutvie|(5025201213)
3|Michael Ariel Manihuruk|(5025201158)

## Soal 1
<br>
1.	Mas Refadi adalah seorang wibu gemink.  Dan jelas game favoritnya adalah bengshin impek. Terlebih pada game tersebut ada sistem gacha item yang membuat orang-orang selalu ketagihan untuk terus melakukan nya. Tidak terkecuali dengan mas Refadi sendiri. Karena rasa penasaran bagaimana sistem gacha bekerja, maka dia ingin membuat sebuah program untuk men-simulasi sistem history gacha item pada game tersebut. Tetapi karena dia lebih suka nge-wibu dibanding ngoding, maka dia meminta bantuanmu untuk membuatkan program nya. Sebagai seorang programmer handal, bantulah mas Refadi untuk memenuhi keinginan nya itu.
<br>
<br>
a.	Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).
<br>
<br>
b.	Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA
<br>
<br>
c.	Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.
<br>
<br>
d.	Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880
<br>
<br>
e.	Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)
<br>

## Pengerjaan
<br>
Program yang dibuat diharapkan dapat berjalan di proses latar belakang, sehingga perlu membuat daemon.

```
pid_t pid, sid;

    pid = fork();

    if(pid < 0){
        exit(EXIT_FAILURE);
    }

    if(pid > 0){
        exit(EXIT_SUCCESS);
    }

    umask(0);

    sid = setsid();

    if(sid < 0){
        exit(EXIT_FAILURE);
    }

    if((chdir("/home/hafizen/modul2/soal1")) < 0){
        exit(EXIT_FAILURE);
    }

    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);
```

### soal 1a
Program mendownload zip databases characters dan weapons. `pid1` untuk menjalankan proses download databases characters dan `pid2` untuk menjalankan proses download databases weapons dimana dengan menggunakan perintah `execv` `wget`.

```
pid_t pid1, pid2;
      int process = 0; 
      pid = fork();
      process++;

      if (pid1 == 0){
        char *args[] = {"wget", "-P","./", "-o-", "-q", "https://drive.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "characters", NULL };
        execv("/bin/wget", args);
      }

      else {
        pid2= fork();
        process++;

        if (pid2== 0){
          char *args[] = {"wget", "-P","./", "-o-", "-q", "https://drive.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "weapons", NULL };
          execv("/bin/wget", args);
        }
```
Melakukan extract zip file databases hasil unduhan(unzip). `pid3` untuk menjalankan proses unzip databases characters dan `pid4` untuk menjalankan proses unzip databases weapons dimana masing-masing menggunakan `execv` `unzip`.
```
pid_t pid3, pid4, pid5;
          pid3 = fork();
          process++;

          if(pid3 == 0){
            char *args[] = {"unzip", "-q", "characters", NULL };
            execv("/bin/unzip", args);
          }

          else{
            pid4 = fork();
            process++;

            if(pid4 == 0){
              char *args[] = {"unzip", "-q", "weapons", NULL };
              execv("/bin/unzip", args);
            }
```
Membuat working space directory `gacha_gacha`. `pid5` untuk menjalankan proses membuat directory `gacha_gacha` dengan menggunakan `execv` `mkdir`.
```
 pid5 = fork();
              process++;

              if(pid5 == 0){
                char *args[] = {"mkdir", "gacha_gacha", NULL };
                execv("/bin/mkdir", args);
              }
```
Menghapus file characters.zip dan weapons.zip yang telah dilakukan unzip dengan menggunakan `remove` function. Kemudian ke working space `gacha_gacha` dengan menggunakan `chdir("./gacha_gacha")`.
```
if (remove("characters.zip") == 0){
                  if(remove("weapons.zip") == 0)
                }

                chdir("./gacha_gacha");
```

### soal 1b
Melakukan gacha weapon bila jumlah gacha yang dilakukan `gacha_counter` merupakan gacha ke genap. Untuk mendapatkan random gacha dengan `rand() % (total_json_weap)`. `total_json_weap` merupakan banyaknya weapon pada databases weapons.
```
if(gacha_counter%2 == 0){
          num = (rand() % (total_json_weap));
          if (num == 0) num++;
          file_weap = popen("find ../weapons", "r");
          for(int i = 0; i < total_json_weap; i++){
            fgets(path, PATH_MAX, file_weap);
            if(i == num) break;
          }
```
Menghitung banyaknya weapon pada databases weapons. dimana disimpan dalam variabel `total_json_weap`. Dimana dilakukan dengan mendapatkan directory weapons `popen` kemudian penghitungan jumlah characters dengan looping hingga `EOF`. 
```
 file_weap = popen("ls ../weapons", "r");
      while((ch=fgetc(file_weap))!=EOF) {
        if(ch=='\n')
          total_json_weap++;
      }
```
Melakukan gacha characters bila jumlah gacha yang dilakukan `gacha_counter` merupakan gacha ke ganjil. Untuk mendapatkan random gacha dengan `rand() % (total_json_weap)`. `total_json_char` merupakan banyaknya characters pada databases characters.
```
 num = (rand() % (total_json_char));
          if (num == 0) num++;
          file_char = popen("find ../characters", "r");
          for(int i = 0; i < total_json_char; i++){
            fgets(path, PATH_MAX, file_char);
            if(i == num) break;
          }
```
Menghitung banyaknya weapon pada databases weapons. dimana disimpan dalam variabel `total_json_char`. Dimana dilakukan dengan mendapatkan directory weapons `popen` kemudian penghitungan jumlah characters dengan looping hingga `EOF`.
```
file_char = popen("ls ../characters", "r");
      while((ch=fgetc(file_char))!=EOF) {
        if(ch=='\n')
          total_json_char++;
      }
```
Membuat file txt baru bila jumlah gacha `gacha_counter` yang dilakukan merupakan telah melewati tepat 1 dari kelipatan 10. 
```
if(gacha_counter % 10 == 1){
          time_t rawtime;
          struct tm * timeinfo;
      
          time(&rawtime);
          timeinfo = localtime(&rawtime);

          memset(str_txt, 0, sizeof str_txt);
          int int_count = ((gacha_counter / 10) + 1) * 10;
          strcat(str_txt, str_dir);
          sprintf(str_temp, "/%02d:%02d:%02d_gacha_%d.txt", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, int_count);
          strcat(str_txt, str_temp);
          sleep(1);
        }
```
Membuat directory baru bila jumlah gacha `gacha_counter` yang dilakukan telah melewati tepat 1 dari kelipatan 90. `pid5` untuk menjalankan proses membuat directory dengan menggunakan  `execv` `mkdir`.
```
if(gacha_counter % 90 == 1){
          pid_t pid5;
          pid5 = fork();

          memset(str_dir, 0, sizeof str_dir);
          int int_count = ((gacha_counter / 90) + 1) * 90;
          sprintf(str_dir,"./total_gacha_%d", int_count);

          if(pid == 0){
            char *args[] = {"mkdir", str_dir, NULL };
            execv("/bin/mkdir", args);
          }
          else {
            while ((wait(&status)) > 0);
          }

        }
```

### soal 1c
Mengatur penamaan file dengan ketentuan `{Hh:Mm:Ss}_gacha_{jumlah-gacha}`.untuk mendapatkan `{Hh:Mm:Ss}` informasi waktu akan disimpan dalam `timeinfo` dimana mendapatkan waktu program berjalan dengan memanfaatkan fungsi waktu `localtime` `time`. agar fungsi waktu dapat berjalan harus menambahkan header `<time.h>`. ketentuan penamaan `{Hh:Mm:Ss}_gacha_{jumlah-gacha}` disimpan dalam string dengan memanfaatkan fungsi string  `sprintf`. Untuk jeda pembuatan 1 detik dilakukan dengan `sleep(1)`. 
```
time_t rawtime;
          struct tm * timeinfo;
      
          time(&rawtime);
          timeinfo = localtime(&rawtime);

          // bikin nama
          memset(str_txt, 0, sizeof str_txt);
          int int_count = ((gacha_counter / 10) + 1) * 10;
          strcat(str_txt, str_dir);
          sprintf(str_temp, "/%02d:%02d:%02d_gacha_%d.txt", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, int_count);
          strcat(str_txt, str_temp);
          sleep(1);
```
Membuat penamaan folder `total_gacha_{jumlah-gacha}`. format penamaan folder dilakukan dengan memanfaatkan fungsi format string `sprintf` kemudian disimpan dalam string `str_dir`.
<br>
Membuat folder directory yang telah diatur formatnya. `pid5` untuk menjalankan proses membuat folder directory dengan memanfaatkan `execv` `mkdir`.
```
pid_t dir_gacha;
          dir_gacha = fork();

          memset(str_dir, 0, sizeof str_dir);
          int int_count = ((gacha_counter / 90) + 1) * 90;
          sprintf(str_dir,"./total_gacha_%d", int_count);

          if(dir_gacha == 0){
            char *args[] = {"mkdir", str_dir, NULL };
            execv("/bin/mkdir", args);
          }
          else {
            while ((wait(&status)) > 0);
          }
```
## soal 1d
Define jumlah `primogems` dengan jumlah `79000`, jumlah gacha `gacha_counter` dengan inisiasi `0`.
```
int gacha_counter = 0, primogems = 79000;
```
Untuk setiap kali gacha dilakukan pengurangan jumlah `primogems` dengan harga setiap gacha adalah `160 primogems`.
```
primogems -= 160;
```
Melakukan parsing json untuk mendapatkan `rarity` dan `name` dari hasil gacha yang didapatkan. Untuk melakukan parsing json dilakukan dengan menginstall dahulu library `ljson-c` kemudian menambahkan header `<json-c/json.h>`.
<br>
Melakukan input data kedalam file txt dengan ketentuan `{jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems` dengan memanfaatkan fungsi `fprintf`.
```
long int size = strlen(path);
          path[strlen(path) - 1] = '\0';

          roll = fopen(path, "r");
          fread(buffer, 3072, 1, roll);
          fclose(roll);

          parsed_json = json_tokener_parse(buffer);
          json_object_object_get_ex(parsed_json, "rarity", &rarity);
          json_object_object_get_ex(parsed_json, "name", &name);

          fprintf(txt_gacha, "%d_weapon_%d_%s_%d\n", gacha_counter, json_object_get_int(rarity), json_object_get_string(name), primogems);
```
### Soal 1e
Menjalankan gacha pada `30 Maret jam 04:44`. Dimana disini untuk mendapatkan waktunya dengan memanfaatkan fasilitas header `<time.h>`. Untuk mendapatkan waktu ketika program berjalan dengan fungsi `time` `localtime` dimana dengan mengatur tanggal 30 `getTime->tm_mday == 30`, Bulan Maret `getTime->tm_mon  == 2`, jam 4 `getTime->tm_hour == 4`, menit 44 `getTime->tm_min  == 44`.
```
        time_t now;
        now = time(NULL);
        struct tm * getTime = localtime(&now);

        if(
            getTime->tm_mday == 30 &&
            getTime->tm_mon  == 2 &&
            getTime->tm_hour == 4 &&
            getTime->tm_min  == 44
        )
```
Mengatur waktu 3 jam setelah program gacha dijalankan pada `30 Maret jam 04:44` dengan memanfaatkan sleep 3 jam dengan 3 dikalikan banyak detik dalam 1 jam( 3 * 3600) `sleep(3 * 3600)`.
```
sleep(3 * 3600);
```
Keluar dari directory `gacha_gacha` dengan `chdir("..")`.
<br>
Melakukan zip folder `gacha_gacha`. `pid6` untuk untuk menjalankan proses zip folder. zip folder `gacha_gacha` disimpan dengan nama `not_safe_for_wibu.zip` dan dengan enkripsi password `satuduatiga` dimana perintah disimpan dalam `char *args[]` kemudian dijalankan dengan menggunakan `execv` `zip`.
```
chdir("..");
      
      pid_t pid6, pid7, pid8, pid9;
      pid6 = fork();
      if(pid6 == 0){
        char *args[] = {"zip", "-q", "-r", "--password", "satuduatiga", "not_safe_for_wibu.zip", "gacha_gacha", NULL };
        execv("/bin/zip", args);
      }
```
Menghapus folder directory characters, weapons, dan gacha_gacha. proses berjalan berurutan dengan `pid7`, `pid8`, `pid9`. Menghapus folder dilakukan dengan menggunakan `execv` `rm`.
```
 pid7 = fork();
        if(pid7 == 0){
          char *args[] = {"rm", "-rf", "characters", NULL };
          execv("/bin/rm", args);
        }
        else {
          pid8 = fork();
          if(pid8 == 0){
            char *args[] = {"rm", "-rf", "weapons", NULL };
            execv("/bin/rm", args);
          }
          else {
            pid9 = fork();
            char *args[] = {"rm", "-rf", "gacha_gacha", NULL };
            execv("/bin/rm", args);
          }
        }
```

## Screenshoot 
Tampilan directory gacha_gacha dan hasil extract databases characters dan weapon.<br>
![101](/uploads/ed4c40e10b7a8076967e4f4c120070f0/101.jpg)<br>
Tampilan folder hasil gacha.<br>
![102](/uploads/2d40f0f79339df6910f2b82d2108e2c3/102.jpg)<br>
Tampilan file txt untuk menyimpan hasil gacha.<br>
![103](/uploads/8cab48dea321a469d6b915585dc63313/103.jpg)<br>
Tampilan hasil gacha yang dilakukan.<br>
![104a](/uploads/cd184ef0d77beb8ab1ac7cdf24f0b639/104a.jpg)<br>
![104b](/uploads/de50adc3de3b42352d730b00e84fc29f/104b.jpg)<br>
Tampilan hasil zip file dari hasil gacha yang telah dilakukan.<br>
![105](/uploads/22690992bdb9a34c8de54e4def4e1543/105.jpg) <br>


## Kendala Pengerjaan

<li> Penyesuaian url untuk mendownload databases.<br>
<li> Mengatur program untuk berjalan pada waktu yang ditentukan.<br>
<li> Bekerja dengan directory yang diinginkan.

___


## Soal 2
### Cara Pengerjaan
**2a.** <br>
Mengextract drakor.zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor” dan menghapus folder-folder yang tidak dibutuhkan. <br>
Pada fungsi main di bawah ini terdapat fungsi untuk membuat folder dengan menggunakan `mkdir`. <br>
```c
int main(){
    pid_t child_id;
	int status;
	child_id = fork();

  	if (child_id == 0){
        char *argv[] = {"mkdir", "-p", "/home/tavilutvie/shift2/drakor", NULL};
    	execv("/bin/mkdir", argv);
    }
	else{
    	((wait(&status)) > 0);
    }
    unzip();
    body_function();
}
```
Kemudian menggunakan fungsi `unzip()` untuk mengekstrak zip ke dalam folder. <br>
```c
void unzip() {
	pid_t child_id;
    int status;
    child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"unzip", "-q", "drakor.zip", "*.png", "-d", "/home/tavilutvie/shift2/drakor", NULL};
		execv("/bin/unzip", argv);
	} 
    else {
		((wait(&status)) > 0);
	}
}
```
`"*.png"` digunakan agar dapat mengekstrak file bertipe png saja. <br>

**2b.** <br>
Membuat folder untuk setiap kategori drakor. <br>
Kami menggunakan fungsi `body_function()` untuk mengerjakan soal b sampai dengan e. <br>
```c
void body_function() {
    char path[] = "/home/tavilutvie/shift2/drakor/";
	struct dirent *dp;
	DIR *dir = opendir(path);
    int m=0;
    while ((dp = readdir(dir)) != NULL) {
		
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0){
            char gambar[1000];
            strcpy(gambar, dp->d_name);
        	strremove(gambar, ".png");
       	 
        	char nama[100], tahun[100], kategori[100];
			int index;
            char *word = strtok(gambar, "_;");
            index = 0;
            while(word != NULL) {
				if(index==3){
					index=0;
				}
                if(index == 2) {
					strcpy(kategori, word);
                    create_folder_recursive(nama, kategori, dp->d_name);

					strcpy(data[m].nama, nama);
					data[m].tahun = atoi(tahun);
					strcpy(data[m].kategori, kategori);
					m++;
                }
				else if(index == 0) {
                 	strcpy(nama, word);
               	}
               	else if(index == 1) {
                 	strcpy(tahun, word);
               	}
               	word = strtok(NULL, "_;");
               	index++;
        	}
			delete_image(dp->d_name);
        }
    }
	ASCsort(data, m);
	add_data1(data, m);
}
```
Fungsi `readdir` digunakan untuk membantu dalam membaca isi directory. Untuk memisahkan nama, tahun, dan kategori dari setiap foto ke dalam masing-masing variabel, digunakan fungsi `strtok()`. <br>
Potongan kode berikut dari fungsi `create_folder_recursive()` berfungsi untuk membuat folder berdasarkan kategorinya.
```c
...
    char newpath[1000];
	char originpath[1000];

	strcpy(newpath, "/home/tavilutvie/shift2/drakor/");
	strcpy(originpath, "/home/tavilutvie/shift2/drakor/");
	strcat(newpath, kategori);

    pid_t child_id;
    int status;
    child_id = fork();
    
    if (child_id == 0){
        char *argv[] = {"mkdir", "-p", newpath, NULL};
        execv("/bin/mkdir", argv);
    }
    else{
        ((wait(&status)) > 0);
    }
...
```

**2c.** <br>
Memindahkan poster ke folder berdasarkan kategori. <br>
Pada fungsi `create_folder_recursive()` terdapat juga potongan kode berikut yang berfungsi untuk menyalin file-file dari `/home/[user]/shift2/drakor/` ke dalam `/home/[user]/shift2/drakor/[kategori]`. <br>
```c
...
strcat(originpath,gambar);
	strcat(newpath, "/");
	strcat(newpath, nama);
	strcat(newpath, ".png");

	child_id = fork();

	if (child_id == 0){
        char *argv[] = {"cp", "-b", originpath, newpath, NULL};
        execv("/bin/cp", argv);
    }
    else{
		((wait(&status)) > 0);
	}
...
```

**2d.** <br>
Satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. <br>
Selanjutnya, untuk memisahkan foto yang terdapat lebih dari satu poster, digunakan variabel bantuan `index`. Apabila `index` telah mencapai angka 3 maka index tersebut akan berubah menjadi semula yaitu 0. <br>
Setelah pengerjaan 1b. sampai dengan 1d. selesai, file-file .png dalam folder drakor/ dihapus dengan menggunakan fungsi `delete_image()`.
```c
void delete_image(char gambar[]) {
	char path[1000];

	strcpy(path, "/home/tavilutvie/shift2/drakor/");
	strcat(path, gambar);

	pid_t child_id;
    int status;
    child_id = fork();

	if (child_id == 0) {
		char *argv[] = {"rm", "-f", path, NULL};
        execv("/bin/rm", argv);
	} 
    else {
		((wait(&status)) > 0);
	}
}
```

**2e.** <br>
Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis dan disort ascending berdasarkan tahun rilis. <br>
Untuk mempermudah sorting, digunakan `struct film` yang berisi nama, tahun, dan kategori. kemudian diurutkan menggunakan fungsi `ASCsort()`.
```c
struct film {
  	char nama[100];
  	int tahun;
  	char kategori[100];
} data[100];

void ASCsort(struct film data[100], int s)
{
    int i, j;
    struct film temp;
    
    for (i = 0; i < s - 1; i++)
    {
        for (j = 0; j < (s - 1-i); j++)
        {
            if (data[j].tahun > data[j + 1].tahun)
            {
                temp = data[j];
                data[j] = data[j + 1];
                data[j + 1] = temp;
            } 
        }
    }
}
```
Setelah berhasil diurutkan, program akan menulis data dari masing-masing kategori menggunakan `add_data1` dan `add_data2`.
```c
void add_data2(char nama[],int tahun,char kategori[]){
    FILE * fp;
	FILE * file;
    char path[1000];

    strcpy(path, "/home/tavilutvie/shift2/drakor/");
    strcat(path, kategori);
    strcat(path, "/data.txt");

	if (file = fopen (path,"r+")) {
		fclose (file);
	}
	else{
		fp = fopen (path,"w+");	
    	fprintf(fp,"kategori : %s\n\n",kategori);

    	fclose (fp);
	}
    fp = fopen (path,"a+");
    fprintf(fp,"nama : %s\nrilis : tahun %d\n\n",nama,tahun);
    fclose (fp);
}

void add_data1(struct film data[100], int m){
	for(int i=0;i<m;i++){
		add_data2(data[i].nama, data[i].tahun, data[i].kategori);
	}
}
```
Untuk membuka dan mengedit data.txt, digunakan fungsi `fopen()` dan `fprintf()`
`if (file = fopen (path,"r+"))` digunakan untuk mengecek adanya isi dari data.txt. Apabila sudah terisi maka tidak perlu dituliskan kategorinya.

### Output
Isi dari forder drakor/ adalah sebagai berikut. <br>
![image](/uploads/1e84a24d2508b852a52f08fc8d7b0152/image.png) <br>
![image](/uploads/b0d471832b0e37ff9fab02a04a9507d1/image.png) <br>
Isi dari /home/[user]/shift2/drakor/action/data.txt adalah sebagai berikut. <br>
![image](/uploads/d70761d48233c18943bb424a22284e69/image.png) <br>

### Kendala
Tidak ada kendala.

## Soal 3
<br>
3.	Conan adalah seorang detektif terkenal. Suatu hari, Conan menerima beberapa laporan tentang hewan di kebun binatang yang tiba-tiba hilang. Karena jenis-jenis hewan yang hilang banyak, maka perlu melakukan klasifikasi hewan apa saja yang hilang
<br>
<br>
a.	Untuk mempercepat klasifikasi, Conan diminta membuat program untuk membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”.
<br>
<br>
b.	Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
<br>
<br>
c.	Tidak hanya itu, hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.
<br>
<br>
d.	Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
<br>
<br>
e.	Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.
Contoh : conan_rwx_hewan.png
<br>

## Pengerjaan
Hal pertama yang harus dilakukan adalah membuat direktori dengan nama `darat`, lalu setelah 3 detik membuat direktori bernama `air`. Kedua folder tersebut berada pada direktori `/home/[USER]/modul2/`. Untuk membuat kedua direktori tersebut dengan jeda 3 detik, dapat dilakukan dengan menggunakan potongan kode berikut. <br>
```c
void createfolderdarat(){
    char *argv[] = {"mkdir" , "-p" , "/home/milkyway/modul2/darat", NULL};
    execv("/bin/mkdir", argv);
}

void createfolderair(){
    char *argv[] = {"mkdir" , "-p" , "/home/milkyway/modul2/air", NULL};
    execv("/bin/mkdir", argv);
}

int main(){
    int  status=0;
    if(fork() < 0){
        exit(EXIT_FAILURE);
    }
    else if(fork() == 0){
        createfolderdarat();
    }
    else{
        int  status1=0;
        while(wait(&status) > 0);
        sleep(3);
        if(fork() == 0){
            createfolderair();
        }

}
```
Setelah itu ekstrak `animal.zip` ke direktori `/home/[USER]/modul2/`. Untuk mengekstraknya, bisa dilakukan menggunakan potongan kode berikut. <br>
```c
void extractzipanimal(){
    char *argv[] = {"unzip" , "-q" , "-o" , "/home/milkyway/modul2/animal.zip", "-d" , "/home/milkyway/modul2" , NULL};
    execv("/bin/unzip", argv);
}
int main(){
    int  status2=0;
            while(wait(&status1) > 0);
            if(fork() == 0){
                extractzipanimal();
            }
}
```
Kemudian hasil ekstrak dipisah ke folder `darat` dan `air` sesuai dengan keterangan yang ada di nama file. Untuk hewan yang tidak memiliki keterangan `darat` maupun `air`, maka akan dihapus. proses ini dapat diselesaikan menggunakan library `dirent.h` untuk mengakses nama setiap file seperti pada potongan kode berikut. <br>
```c
void movedarat(){
    char *argv[] = {"find" , "/home/milkyway/modul2/animal" , "-name" , "*darat*", "-exec" , "mv" , "-t" , "/home/milkyway/modul2/darat" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

void moveair(){
    char *argv[] = {"find" , "/home/milkyway/modul2/animal" , "-name" , "*air*", "-exec" , "mv" , "-t" , "/home/milkyway/modul2/air" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

void deletewithoutstatus(){
    char *argv[] = {"find" , "/home/milkyway/modul2/animal" , "-name" , "*jpg", "-exec" , "rm" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

int main(){
    int  status3=0;
    while(wait(&status2) > 0);
    if(fork() == 0){
          movedarat();
          }else{
              int  status4=0;
              while(wait(&status3) > 0);
              if(fork() == 0){
                  moveair();
                  }else{
                      int  status5=0;
                      while(wait(&status4) > 0);
                      if(fork() == 0){
                      deletewithoutstatus();
                    }
}
```
Lalu, di direktori `darat`, dihapus semua file yang memiliki keterangan `bird`. proses ini dapat diselesaikan menggunakan potongan kode berikut.
```c
void deletebird(){
    char *argv[] = {"find" , "/home/milkyway/modul2/darat" , "-name" , "*bird*", "-exec" , "rm" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

int main(){
    int  status6=0;
      while(wait(&status5) > 0);
      if(fork() == 0){
          deletebird();
        }

}
```
Terakhir, di direktori air, dibuat file `list.txt` yang menampung seluruh data file di direktori tersebut dengan format `UID_UID File Permission_File name.[jpg/png]`. Untuk melakukan proses tersebut, bisa dilakukan dengan potong kode berikut.
```c
void listfile(){
    FILE *filelist = fopen("/home/milkyway/modul2/air/list.txt" , "w");
    if(filelist == NULL){
        printf("File Not Found !");
    }
    DIR *filedir;
    struct dirent *entryfile;
    filedir = opendir("/home/milkyway/modul2/air");
    if(filedir == NULL){
        printf("Directory Not Found !");
    }
    while(entryfile = readdir(filedir)){
        uid_t username = getuid();
        register struct passwd *findusername;
        findusername = getpwuid(username);
        char permission[5] = "";
        struct stat filestatus;
        int r = stat("/home/milkyway/modul2/air" , &filestatus);
        if(r == -1){
            printf("Status Error !");
        }
        if(strcmp(entryfile->d_name , ".") != 0 && strcmp(entryfile->d_name , "..") != 0 && strcmp(entryfile->d_name , "list.txt") != 0){
             if( filestatus.st_mode & S_IRUSR ){
                strcat(permission , "r");
            }
            if( filestatus.st_mode & S_IWUSR ){
                strcat(permission , "w");
            }
            if( filestatus.st_mode & S_IXUSR ){
                strcat(permission , "x");
            }
            fprintf(filelist , "%s_%s_%s\n", findusername->pw_name , permission , entryfile->d_name);
        }
    }
    fclose(filelist);
}

int main(){
    while(wait(&status6) > 0);
    listfile();
}
```


### Output
Isi dari folder `darat` adalah sebagai berikut. <br>

![image](gambar/darat.png) <br>

Isi dari folder `air` adalah sebagai berikut. <br>

![image](gambar/air.png) <br>

Isi dari file `list.txt` adalah sebagai berikut. <br>

![image](gambar/list.png) <br>

Isi dari folder `animal` di akhir eksekusi adalah sebagai berikut. <br>

![image](gambar/animal.png) <br>

### Kendala
Tidak ada kendala.
