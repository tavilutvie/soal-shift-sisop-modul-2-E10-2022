#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<wait.h>
#include<dirent.h>
#include<unistd.h>
#include<sys/stat.h>
#include<pwd.h>

void createfolderdarat(){
    char *argv[] = {"mkdir" , "-p" , "/home/milkyway/modul2/darat", NULL};
    execv("/bin/mkdir", argv);
}

void createfolderair(){
    char *argv[] = {"mkdir" , "-p" , "/home/milkyway/modul2/air", NULL};
    execv("/bin/mkdir", argv);
}

void extractzipanimal(){
    char *argv[] = {"unzip" , "-q" , "-o" , "/home/milkyway/modul2/animal.zip", "-d" , "/home/milkyway/modul2" , NULL};
    execv("/bin/unzip", argv);
}

void movedarat(){
    char *argv[] = {"find" , "/home/milkyway/modul2/animal" , "-name" , "*darat*", "-exec" , "mv" , "-t" , "/home/milkyway/modul2/darat" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

void moveair(){
    char *argv[] = {"find" , "/home/milkyway/modul2/animal" , "-name" , "*air*", "-exec" , "mv" , "-t" , "/home/milkyway/modul2/air" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

void deletewithoutstatus(){
    char *argv[] = {"find" , "/home/milkyway/modul2/animal" , "-name" , "*jpg", "-exec" , "rm" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

void deletebird(){
    char *argv[] = {"find" , "/home/milkyway/modul2/darat" , "-name" , "*bird*", "-exec" , "rm" , "{}" , "+" , NULL};
    execv("/bin/find", argv);
}

void listfile(){
    FILE *filelist = fopen("/home/milkyway/modul2/air/list.txt" , "w");
    if(filelist == NULL){
        printf("File Not Found !");
    }
    DIR *filedir;
    struct dirent *entryfile;
    filedir = opendir("/home/milkyway/modul2/air");
    if(filedir == NULL){
        printf("Directory Not Found !");
    }
    while(entryfile = readdir(filedir)){
        uid_t username = getuid();
        register struct passwd *findusername;
        findusername = getpwuid(username);
        char permission[5] = "";
        struct stat filestatus;
        int r = stat("/home/milkyway/modul2/air" , &filestatus);
        if(r == -1){
            printf("Status Error !");
        }
        if(strcmp(entryfile->d_name , ".") != 0 && strcmp(entryfile->d_name , "..") != 0 && strcmp(entryfile->d_name , "list.txt") != 0){
             if( filestatus.st_mode & S_IRUSR ){
                strcat(permission , "r");
            }
            if( filestatus.st_mode & S_IWUSR ){
                strcat(permission , "w");
            }
            if( filestatus.st_mode & S_IXUSR ){
                strcat(permission , "x");
            }
            fprintf(filelist , "%s_%s_%s\n", findusername->pw_name , permission , entryfile->d_name);
        }
    }
    fclose(filelist);
}

int main(){
    int  status=0;
    if(fork() < 0){
        exit(EXIT_FAILURE);
    }
    else if(fork() == 0){
        createfolderdarat();
    }
    else{
        int  status1=0;
        while(wait(&status) > 0);
        sleep(3);
        if(fork() == 0){
            createfolderair();
        }
        else{
            int  status2=0;
            while(wait(&status1) > 0);
            if(fork() == 0){
                extractzipanimal();
            }
            else{
                int  status3=0;
                while(wait(&status2) > 0);
                if(fork() == 0){
                    movedarat();
                }else{
                    int  status4=0;
                    while(wait(&status3) > 0);
                    if(fork() == 0){
                        moveair();
                    }else{
                        int  status5=0;
                        while(wait(&status4) > 0);
                        if(fork() == 0){
                            deletewithoutstatus();
                        }else{
                            int  status6=0;
                            while(wait(&status5) > 0);
                            if(fork() == 0){
                                deletebird();
                            }else{
                                while(wait(&status6) > 0);
                                listfile();
                            }
                        }
                    }
                }
            }
        }
    }
    
}

